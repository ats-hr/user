# User management bundle

## Install

### Requirements:
Global

- PHP8
- Doctrine ODM

Api

- Nelmio ApiDoc
- JMS Serializer

composer.json settings:
````
    "require": {
        "melon/user": "*"
    },
    "repositories": [
        {
            "type": "vcs",
            "url": "https://gitlab.webery.com/melon/user.git"
        }
    }
````
````
composer require melon/user
````
##Default user management
