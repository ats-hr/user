<?php


namespace AtsHr\User\Exception;


use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Throwable;

class UserAlreadyExistsException extends AuthenticationException
{
    /**
     * @return mixed
     */
    public function getMessageKey()
    {
        return 'User already exists';
    }
}
