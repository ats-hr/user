<?php


namespace AtsHr\User\Exception;


use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Throwable;

class UserMissingDataException extends AuthenticationException
{
    public function getMessageKey()
    {
        return 'Missing user email or password';
    }
}
