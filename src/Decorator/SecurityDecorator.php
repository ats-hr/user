<?php
declare(strict_types=1);

namespace AtsHr\User\Decorator;


use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Psr\Container\ContainerInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationCredentialsNotFoundException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class SecurityDecorator
 *
 * @package Probond\UserBundle\Decorator
 */
class SecurityDecorator extends Security
{
    /**
     * @var bool
     */
    public bool $systemUser = true;

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    private string $secret;

    /**
     * @var array|string[]
     */
    private array $roles;

    /**
     * SecurityDecorator constructor.
     *
     * @param DocumentManager    $dm
     * @param ContainerInterface $container
     */
    public function __construct(
        protected DocumentManager $dm,
        ContainerInterface $container
    ) {
        parent::__construct($container);
        $this->secret = getenv('APP_SECRET');
//        $this->user = null;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?UserInterface
    {
        if ($this->user instanceof User) {
            return $this->user;
        }

        /** @var User $user */
        $user = parent::getUser();

        if (!$user and $this->systemUser === true) {
            $user = $this->getSystemUser();
        } else {
            $this->user = $user;
        }

        return $user;
    }

    /**
     * Checks if the attributes are granted against the current authentication token and optionally supplied subject.
     *
     * @param mixed $attributes
     * @param mixed $subject
     *
     * @return bool
     */
    public function isGranted($attributes, $subject = null): bool
    {
        try {
            if (parent::isGranted('ROLE_SUPER_ADMIN', $subject)) {
                return true;
            }

            return parent::isGranted($attributes, $subject);
        } catch (AuthenticationCredentialsNotFoundException $e) {
            return $this->getUser() && $this->getUser()->hasRole($attributes);
        }
    }

    /**
     * @return User|null
     */
    public function getSystemUser(): User|null
    {
//        $this->dm->getFilterCollection()->disable('softdeleteable');
        return $this->dm->getRepository(User::class)
            ->findOneBy(['username' => 'system']);
    }

    /**
     * @param bool $systemUser
     *
     * @return SecurityDecorator
     */
    public function setSystemUser(bool $systemUser): SecurityDecorator
    {
        $this->systemUser = $systemUser;

        return $this;
    }

    /**
     * @param User $user
     *
     * @return SecurityDecorator
     */
    public function setUser(User $user): SecurityDecorator
    {
        $this->user = $user;
        $this->roles = $user->getRoles();

        return $this;
    }

    /**
     * @return $this
     */
    public function resetUser(): SecurityDecorator
    {
        $this->user = null;

        return $this;
    }

    /**
     * @param string $data
     *
     * @return null|string
     */
    public static function encrypt(string $data): ?string
    {
        $key = getenv('APP_SECRET');
        $length = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $strong = null;
        $iv = openssl_random_pseudo_bytes($length, $strong);
        $ciphertext_raw = openssl_encrypt(
            $data,
            $cipher,
            $key,
            $options = OPENSSL_RAW_DATA, $iv);

        $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);
        $encrypted = strtr(base64_encode(
            $iv . $hmac . $ciphertext_raw
        ), '+/', '-_');

        return $encrypted;
    }

    /**
     * @param string $data
     *
     * @return string
     */
    public function decrypt($data)
    {
        $decrypted = false;
        $key = getenv('APP_SECRET');

        /** OpenSSL Decrypter */
        $c = \base64_decode(\strtr($data, '-_', '+/'), true);
        $length = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = substr($c, 0, $length);
        $hmac = substr($c, $length, $sha2len = 32);
        $ciphertext_raw = substr($c, $length + $sha2len);
        $original_plaintext = openssl_decrypt(
            $ciphertext_raw,
            $cipher,
            $key,
            $options = OPENSSL_RAW_DATA,
            $iv
        );

        $string = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary = true);

        if (hash_equals($hmac, $string))//PHP 5.6+ timing attack safe comparison
        {
            $decrypted = $original_plaintext;
        }


        return trim($decrypted);
    }
}
