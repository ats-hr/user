<?php
declare(strict_types=1);

namespace AtsHr\User\DataFixtures;

use App\Document\User;
use Doctrine\Bundle\MongoDBBundle\Fixture\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserFixture
 * @package AtsHr\User\DataFixtures
 */
class UserFixture extends Fixture
{
    public function __construct(
        private UserPasswordEncoderInterface $encoder)
    {}

    /**
     * Load data fixtures with the passed EntityManager
     */
    public function load(ObjectManager $manager)
    {
        $admin = User::create()
            ->setUsername('admin')
            ->setEmail('admin@melon.dev')
            ->addRole('ROLE_SUPER_ADMIN')
            ->setEnabled(true)
        ;
        $admin
            ->setPassword($this->encoder->encodePassword(
                $admin,
                'Abcd1234'
            ))
        ;

        $manager->persist($admin);

        $system = User::create()
            ->setUsername('system')
            ->setEmail('no-reply@melon.dev')
            ->addRole('ROLE_SUPER_ADMIN')
            ->setEnabled(false)
        ;
        $manager->persist($system);

        $manager->flush();
    }
}
