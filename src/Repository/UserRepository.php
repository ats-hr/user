<?php
declare(strict_types=1);

namespace AtsHr\User\Repository;

use App\Document\User;
use Doctrine\ODM\MongoDB\LockException;
use Doctrine\ODM\MongoDB\LockMode;
use Doctrine\ODM\MongoDB\Mapping\MappingException;
use Doctrine\ODM\MongoDB\MongoDBException;
use Doctrine\Bundle\MongoDBBundle\{ManagerRegistry, Repository\ServiceDocumentRepository};
use Symfony\Bridge\Doctrine\Security\User\UserLoaderInterface;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceDocumentRepository implements UserLoaderInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    /**
     * @param string $username
     * @return User|UserInterface|null
     */
    public function loadUserByUsername(string $username)
    {
        return $this->findOneBy(['email' => $username]);
    }

    /**
     * @param mixed $id
     * @param int $lockMode
     * @param int|null $lockVersion
     * @return User|null
     * @throws LockException
     * @throws MappingException
     */
    public function find($id, int $lockMode = LockMode::NONE, ?int $lockVersion = null): ?object
    {
        if (\is_string($id) and preg_match('/@/i', $id)) {
            return $this->loadUserByUsername($id);
        }

        return parent::find($id, $lockMode, $lockVersion);
    }


    /**
     * @param User $user
     * @return User
     * @throws MongoDBException|\InvalidArgumentException
     */
    public function save(User $user)
    {
        $dm = $this->getDocumentManager();
        if (!$user->getCreatedAt()) {
            $dm->persist($user);
        }
        $dm->flush();

        return $user;
    }
}
