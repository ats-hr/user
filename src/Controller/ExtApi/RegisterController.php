<?php
declare(strict_types=1);

namespace AtsHr\User\Controller\ExtApi;

use App\Document\User;
use AtsHr\User\Exception\UserAlreadyExistsException;
use AtsHr\User\Exception\UserMissingDataException;
use AtsHr\User\Repository\UserRepository;
use App\Request\BodyRequest;
use Doctrine\ODM\MongoDB\MongoDBException;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationFailureHandler;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Http\Authentication\AuthenticationSuccessHandler;
use AtsHr\Helper\JMSSerializerHelper;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\UriSafeTokenGenerator;

/**
 * Class RegisterController
 * @package App\Controller
 *
 * @OA\Tag(name="registration")
 */
class RegisterController extends AbstractController
{

    /**
     * @Route("/api/user/register", methods={"POST"}))
     *
     * @OA\Post(
     *     summary="Create new user",
     *     description="A /email/check?email=1@2.3 hívásával lehet ellenőrizni, hogy a megadott email létezik-e.",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="email",type="string",),
     *             @OA\Property(property="fullName",type="string",),
     *             @OA\Property(property="password",type="string",),
     *         )
     *     )
     * )
     * @OA\Response(
     *     response=200, description="",
     *     @OA\JsonContent(
     *         @OA\Property(property="token", type="string"),
     *         @OA\Property(property="refresh_token", type="string"),
     *         @OA\Property(property="meta", type="object",
     *              @OA\Property(property="user", ref=@Model(type=User::class, groups={"profile"})),
     *              @OA\Property(property="registration", type="string", example="SUCCESS"),
     *         ),
     *     )
     * )
     *
     *
     * @param BodyRequest $request
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $encoder
     * @param AuthenticationSuccessHandler $successHandler
     * @param AuthenticationFailureHandler $failureHandler
     * @return Response
     * @throws MongoDBException
     * @throws \InvalidArgumentException
     */
    public function registration(
        BodyRequest $request,
        UserRepository $userRepository,
        UserPasswordEncoderInterface $encoder,
        AuthenticationSuccessHandler $successHandler,
        AuthenticationFailureHandler $failureHandler
    ): Response
    {
        if ($request->get('password') and $request->get('email')) {
            $testUser = $userRepository->loadUserByUsername($request->get('email'));
            if ($testUser) {
                return $failureHandler->onAuthenticationFailure($request, new UserAlreadyExistsException());
            }

            $user = User::create()
                ->setUsername('reg-'.time().rand(11,99))
                ->setEmail($request->get('email'))
                ->setFullName($request->get('fullName'))
                ->setConfirmationToken((new UriSafeTokenGenerator())->generateToken())
                ->setEnabled(true)
                ->setHistoryEvent('REGISTRATION - EMAIL')
            ;
            $user->setPassword($encoder->encodePassword($user, $request->get('password')));

            $userRepository->save($user);

            /** @var JsonResponse $response */
            $response = $successHandler->handleAuthenticationSuccess($user);
            $tokens = json_decode($response->getContent(), true );

            $tokens['meta'] = [
                'user' => JMSSerializerHelper::serialize($user, 'profile'),
                'registration' => "SUCCESS"
            ];


            $response->setData($tokens);

            return $response;
        }

        return $failureHandler->onAuthenticationFailure($request, new UserMissingDataException());
    }
}
