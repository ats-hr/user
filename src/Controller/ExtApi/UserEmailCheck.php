<?php
declare(strict_types=1);

namespace AtsHr\User\Controller\ExtApi;

use App\Document\User;
use App\Repository\UserRepository;
use App\Request\BodyRequest;
use Doctrine\ODM\MongoDB\DocumentManager;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class UserController
 * @package App\Controller
 *
 * method User getUser()
 * @OA\Tag(name="registration")
 */
class UserEmailCheck extends AbstractController
{
    /**
     * @Route(
     *     name="api_user_email_check",
     *     path="/email/check",
     *     methods={"GET", "POST"},
     * )
     * @OA\Get(description="Ellenőrzi az email címet, hogy létezik-e már a rendszerben")
     * @OA\Post(description="Ellenőrzi az email címet, hogy létezik-e már a rendszerben")
     * @OA\Parameter(name="email", in="query")
     * @OA\Response(response=200,description="Email found")
     * @OA\Response(response=404,description="Email not found")
     *
     * @throws \TypeError
     */
    public function check(BodyRequest $request, DocumentManager $dm): JsonResponse
    {
        $repository = $dm->getRepository(User::class);
        $user = $repository->findOneBy(['email' => $request->get('email')]);
        return new JsonResponse(['status' => $user ? 200 : 404], $user ? 200 : 404);
    }
}
