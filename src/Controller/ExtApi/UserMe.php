<?php
declare(strict_types=1);

namespace AtsHr\User\Controller\ExtApi;

use App\Document\User;
use App\Document\UserData\UserSettings;
use App\Request\BodyRequest;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use AtsHr\Helper\JMSSerializerHelper;
use Melon\Contracts\{Document\DataSource\Country, Document\Embeddable\Address, Messages\FileMessage};
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as OA;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpStamp;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * Class UserController
 * @package App\Controller
 *
 * @method User getUser()
 * @OA\Tag(name="/me", description="User related paths")
 */
class UserMe extends AbstractController
{
    /**
     * @Route(
     *     name="api_user_me",
     *     path="/api/user/me",
     *     methods={"GET"},
     * )
     * @OA\Get(summary="Get my data")
     * @OA\Response(
     *     response=200, description="",
     *     @Model(type=User::class, groups={"profile"})
     * )
     */
    public function me(Request $request)
    {
        return new JsonResponse(JMSSerializerHelper::serialize(
            $this->getUser(),
            $request->get('groups', ['profile'])
        ));
    }

    /**
     * @Route(
     *     path="/api/user/me",
     *     methods={"POST"},
     * )
     * @OA\Post(
     *     summary="Create new user",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="profile",type="object",
     *                 @OA\Property(property="email",type="string"),
     *                 @OA\Property(property="full_name",type="string"),
     *                 @OA\Property(property="phone",type="string"),
     *                 @OA\Property(property="location",type="string"),
     *             ),
     *             @OA\Property(property="company",type="object",
     *                 @OA\Property(property="name",type="string"),
     *                 @OA\Property(property="tax_number",type="string"),
     *                 @OA\Property(property="zip_code",type="string"),
     *                 @OA\Property(property="township",type="string"),
     *                 @OA\Property(property="country",type="string", description="hash!"),
     *                 @OA\Property(property="address",type="string"),
     *                 @OA\Property(property="address2",type="string"),
     *             ),
     *             @OA\Property(property="billing_info",type="object",
     *                 @OA\Property(property="is_company",type="boolean"),
     *                 @OA\Property(property="name",type="string"),
     *                 @OA\Property(property="tax_number",type="string"),
     *                 @OA\Property(property="zip_code",type="string"),
     *                 @OA\Property(property="township",type="string"),
     *                 @OA\Property(property="country",type="string", description="hash!"),
     *                 @OA\Property(property="address",type="string"),
     *                 @OA\Property(property="address2",type="string"),
     *             ),
     *             @OA\Property(property="settings",type="object",
     *                 @OA\Property(property="help",type="boolean"),
     *                 @OA\Property(property="notifications",type="boolean"),
     *             ),
     *         )
     *     )
     * )
     * @OA\Response(
     *     response=200, description="",
     *     @Model(type=User::class, groups={"credit", "profile", "company", "address"})
     * )
     * @param BodyRequest $request
     * @param DocumentManager $dm
     * @return JsonResponse
     * @throws MongoDBException
     */
    public function update(
        BodyRequest $request,
        DocumentManager $dm
    ): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();
        $company = $user->getActiveCompany();

        if ($profile = $request->get('profile')) {
            $user->setEmail($profile['email'] ?? null );
            $user->setFullName($profile['full_name'] ?? null );
            $user->setPhone($profile['phone'] ?? null );
            $user->setLocation($profile['location'] ?? null );
        }

        if($companyData = $request->get('company')) {
            $country = isset($companyData['country']) ? $dm->find(Country::class, $companyData['country']) : false;

            $company
                ->setName($companyData['name'] ?? null )
                ->setTaxNumber($companyData['tax_number'] ?? null )
                ->setAddress((new Address())
                    ->setCountry($country ?? null)
                    ->setZipCode($companyData['zip_code'] ?? null)
                    ->setTownship($companyData['township'] ?? null)
                    ->setAddress($companyData['address'] ?? null)
                    ->setAddress2($companyData['address2'] ?? null)
                );
        }

        if ($billing = $request->get('billing_info')) {
            $country = isset($billing['country']) ? $dm->find(Country::class, $billing['country']) : false;
            $company
                ->setBillingCompany($billing['is_company'] ?? false )
                ->setBillingName($billing['name'] ?? null )
                ->setBillingTaxNumber($billing['tax_number'] ?? null )
                ->setBillingAddress((new Address())
                    ->setCountry($country ?? null)
                    ->setZipCode($billing['zip_code'] ?? null)
                    ->setTownship($billing['township'] ?? null)
                    ->setAddress($billing['address'] ?? null)
                    ->setAddress2($billing['address2'] ?? null)
                );
        }

        if ($settings = $request->get('settings')) {
            $user->setSettings(
                (new UserSettings())
                    ->setHelp($settings['help'] ?? false )
                    ->setNotifications($settings['notifications'] ?? false )
            );
        }

        $dm->flush();

        return new JsonResponse(JMSSerializerHelper::serialize(
            $this->getUser(),
            ['profile', 'credit', 'company', 'address']
        ));
    }

    /**
     * @Route("/api/user/me/chpw",methods={"POST"})
     * @OA\Post(
     *     summary="Chane password",
     *     @OA\RequestBody(
     *         description="Input data format",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="password",type="string")
     *         )
     *     )
     * )
     * @OA\Response(response=200, description="Success")
     * @OA\Response(response=500, description="Missing password")
     *
     * @param BodyRequest $request
     * @param UserPasswordEncoderInterface $encoder
     * @param DocumentManager $dm
     * @return JsonResponse
     * @throws MongoDBException
     */
    public function chpw(
        BodyRequest $request,
        UserPasswordHasherInterface $encoder,
        DocumentManager $dm
    )
    {
        if ($request->get('password')) {
            $user = $this->getUser();

            $user->setPassword(
                $encoder->encodePassword(
                    $user,
                    $request->get('password')
                )
            );
            $user->setHistoryEvent('CHANGE_PASSWORD');

            $dm->flush();

            return new JsonResponse(true);
        }

        throw new \Exception('Missing password');
    }

    /**
     * @Route("/api/user/me/upload", methods={"POST"})
     * @OA\Post(
     *     summary="Upload profile",
     *     @OA\RequestBody(
     *         description="Input data form",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                  @OA\Property(description="The file input", property="image",type="binary",),
     *             )
     *         )
     *     )
     * )
     */
    public function upload(
        BodyRequest $request,
        MessageBusInterface $bus
    )
    {
        /** @var UploadedFile $file */
        $file = $request->files->get('image');

        $file = $file->move($_ENV['ATTACHMENTS_DIR'], $file->getClientOriginalName());


        $fm = (new FileMessage())
            ->setName($file->getBasename())
            ->setUrl($file->getPathname())
            ->setOwnerClass(User::class)
            ->setOwnerId($this->getUser()->getId())
            ->setTags(['profile_image'])
            ->setReplace(true)
        ;

        $bus->dispatch($fm, [new AmqpStamp()]);

        return new JsonResponse(['saved'=>true]);
    }
}
