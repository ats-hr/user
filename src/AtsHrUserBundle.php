<?php
declare(strict_types=1);

namespace AtsHr\User;

use AtsHr\User\DependencyInjection\AtsHrUserExtension;
use Doctrine\Bundle\MongoDBBundle\DependencyInjection\DoctrineMongoDBExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AtsHrUserBundle extends Bundle
{
    /**
     * {@inheritDoc}
     */
    public function getContainerExtension()
    {
        return new AtsHrUserExtension();
    }
}
