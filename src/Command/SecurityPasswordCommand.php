<?php
declare(strict_types=1);


namespace AtsHr\User\Command;


use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * Class SecurityPasswordCommand
 * @package AtsHr\User\Command
 * @version 1.0
 */
class SecurityPasswordCommand extends Command
{
    protected static $defaultName = "security:user:change-password";
    /**
     * @var SymfonyStyle
     */
    private $ui;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var DocumentManager
     */
    private $dm;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        DocumentManager $dm,
        string $name = null
    ) {
        parent::__construct($name);
        $this->encoder = $encoder;
        $this->dm = $dm;
    }

    protected function configure()
    {
        $this->addArgument("user", InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
            $us = $this->dm->getRepository(User::class)
            ->findBy(["email" => "ptibor@gmail.com"]);


        foreach ($us as $u) {
            dump($u->getEmail());
        }

        $ui = new SymfonyStyle($input, $output);
        $helper = new SymfonyQuestionHelper();

        if ($input->getArgument('user')) {
            $username = $input->getArgument('user');
        } else {
            $username = $helper->ask($input, $output, new Question('Enter user email'));
        }

        dump(User::class);

        /** @var User $user */
        $user = $this->dm->getRepository(User::class)
            ->findOneBy(['email' => trim($username)]);

        if (!$user) {
            throw new \Exception('Unknown user');
        }

        dump($user);
        $password1 = $helper->ask($input, $output, (new Question('Enter password'))->setHidden(true));
        $password2 = $helper->ask($input, $output, (new Question('Enter password again'))->setHidden(true));

        if ($user and $password1 === $password2) {
            $user->setPassword(
                $this->encoder->encodePassword(
                    $user,
                    $password1
                )
            );
            $user->setHistoryEvent('CHANGE_PASSWORD');
            $this->dm->flush();
        }

        $ui->success("Password changed");

        return self::SUCCESS;
    }
}
