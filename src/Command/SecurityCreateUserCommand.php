<?php
declare(strict_types=1);


namespace AtsHr\User\Command;


use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Symfony\Bundle\SecurityBundle\DependencyInjection\Security\UserProvider\UserProviderFactoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\SymfonyQuestionHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Validator\Constraints\Email;

/**
 * Class SecurityCreateUserCommand
 * @package AtsHr\User\Command
 * @version 1.0
 */
class SecurityCreateUserCommand extends Command
{
    protected static $defaultName = "security:user:create";
    /**
     * @var SymfonyStyle
     */
    private $ui;
    /**
     * @var UserPasswordEncoderInterface
     */
    private $encoder;
    /**
     * @var DocumentManager
     */
    private $dm;

    public function __construct(
        UserPasswordEncoderInterface $encoder,
        DocumentManager $dm,
        string $name = null
    )
    {
        parent::__construct($name);
        $this->encoder = $encoder;
        $this->dm = $dm;
    }

    protected function configure()
    {
        $this->addArgument("user", InputArgument::OPTIONAL);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $ui = new SymfonyStyle($input, $output);
        $helper = new SymfonyQuestionHelper();

        if ($input->getArgument('user')) {
            $username = $input->getArgument('user');
        } else {
            $username = $helper->ask($input, $output, new Question('Enter user name'));
        }

        /** @var User $user */
        $user = $this->dm->getRepository(User::class)
            ->findOneBy(['username' => trim($username)]);

        if ($user) {
            $ui->error("User $username already exists!");
            exit;
        }

        $email    = $helper->ask($input, $output, (new Question('Enter email')));

        /** @var User $user */
        $user = $this->dm->getRepository(User::class)
            ->findOneBy(['email' => $email]);

        if ($user) {
            $ui->error("User with email address, $email already exists!");
            exit;
        }

        $password = $helper->ask($input, $output, (new Question('Enter password'))->setHidden(true));
        $name     = $helper->ask($input, $output, (new Question('Enter full name')));
        $location = $helper->ask($input, $output, (new Question('Enter location')));

        $user = User::create()
            ->setUsername($username)
            ->setEmail($email)
            ->setFullName($name)
            ->setLocation($location)
        ;

        $password = $this->encoder->encodePassword($user, $password);
        $user->setPassword($password);

        $this->dm->persist($user);
        $this->dm->flush();

        $ui->success("Created user: $username");

        return 1;
    }
}
