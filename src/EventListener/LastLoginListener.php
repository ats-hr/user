<?php
declare(strict_types=1);

namespace AtsHr\User\EventListener;

use App\Document\User;
use Doctrine\ODM\MongoDB\DocumentManager;
use Doctrine\ODM\MongoDB\MongoDBException;
use Melon\Contracts\Interfaces\EventListenerInterface;
use Symfony\Component\Security\Core\Event\AuthenticationSuccessEvent;

/**
 * Class LastLoginListener
 * @package AtsHr\User\EventListener
 */
class LastLoginListener implements EventListenerInterface
{
    public function __construct(
        private DocumentManager $dm)
    {}

    /**
     * @param AuthenticationSuccessEvent $event
     * @throws MongoDBException
     */
    public function __invoke(AuthenticationSuccessEvent $event)
    {
        $user = $event->getAuthenticationToken()->getUser();
        if ($user instanceof User) {
            $user
                ->setLastLogin(new \DateTime())
            ;
            $this->dm->flush();
        }
    }
}
