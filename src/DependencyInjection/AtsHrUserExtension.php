<?php
declare(strict_types=1);


namespace AtsHr\User\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\Yaml\Yaml;

/**
 * Class MelonUserExtension
 * @package AtsHr\User\DependencyInjection
 */
class AtsHrUserExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     * @throws \InvalidArgumentException
     *
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/../../config'));
        $loader->load('services.yaml');

        # load config.yaml
        try {
            $yaml = Yaml::parseFile($loader->getLocator()->locate('config.yaml'));
            if ($yaml) {
                foreach ($yaml as $extension => $config) {
                    $container->prependExtensionConfig($extension, $config);
                }
            }
        } catch (\Exception $e) {

        }
    }
}
