<?php
declare(strict_types=1);

namespace App\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;
use AtsHr\User\Document\User as BaseUser;

/**
 * Class User
 * @package App\Document
 *
 * @MongoDB\Document(repositoryClass="AtsHr\User\Repository\UserRepository")
 */
class User extends BaseUser
{

}
